#nodes.pp文件定义各个节点应用哪些类（class），也就是安装什么软件及配置什么文件；
#######################################################
#定义一个base类，作为其他节点的初始化配置！
#######################################################

class base {
     include init
     include yum
     include file::bsh
     include file::sbin
     include user::nagios
     include snmpd
     include selinux::disabled
     include ntpdate
     include logclient
}



#####################################
#          以下为具体的节点                                                                                                                                                     #
####################################

node 'alarm0.h3.jl' {
     include init
     include yum
     include file::bsh
     include file::sbin
     include user::nagios
     include snmpd
     include selinux::disabled
     include ntpdate
}



node 'alarm1.h3.jl' {
     include init
     include yum
     include file::bsh
     include file::sbin
     include user::nagios
     include snmpd
     include selinux::disabled
}

node 'pxe.h3.jl' {
     include init
     include yum
     include file::bsh
     include file::sbin
     include user::nagios
     include snmpd
     include selinux::disabled
     include ntpdate
     include logclient
}

node 'h3file1.h3.jl' {
     include init
     include yum
     include file::bsh
     include file::sbin
#    include user::nagios
     include snmpd
     include selinux::disabled
     include ntpdate
     include logclient
}
node 'h3web-standby1-telecom.h3.jl' {
     include base
#     include user::nginx
#     include user::tomcat
}


node 'tv365web1.h3.jl','tv365web2.h3.jl' {
      include base
#     include user::nginx
#     include user::tomcat
}

node 'powermax1.h3.jl' {
      include base
      include user::nginx
      include user::tomcat
}

node 'pxetest.h3.jl' {
      include base
      include user::nginx
      include user::tomcat
      include sudo
}

node 'routon1.h3.jl','routon2.h3.jl' {
     include init
     include yum
     include file::bsh
     include file::sbin
#    include user::nagios
     include snmpd
     include selinux::disabled
     include ntpdate
     include logclient
#    include user::nginx
#    include user::tomcat
}

node 'h3mg1.h3.jl' {
      include base
#     include user::nginx
#     include user::tomcat
}


node 'h3mg2.h3.jl' {
      include base
     include user::nginx
     include user::tomcat
}

node 'h3web1.h3.jl','h3web2.h3.jl' {
      include base
#     include user::nginx
#     include user::tomcat
}

node 'lvs1.h3.jl','lvs2.h3.jl' {
      include base
      include keepalived
}



node 'rfdata1.h3.jl','rfdata2.h3.jl' {
      include base
}


node 'ad01.h3.jl' {
      include base
}

node 'gate1.h3.jl','gate2.h3.jl' {
      include base
      include user::tomcat
}
#########################################
node 'node29.puppet.com'{
      include base
      include snmpd
      include ntpdate
      include logclient
#      include keepalived
}




