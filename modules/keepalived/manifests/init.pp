import "*.pp"

class keepalived {
   case $hostname {
        lvs1: {
            file {
            "/etc/keepalived/keepalived.conf":
            mode  => 644,
            owner => root,
            group => root,
            ensure => present,
            source => "puppet://$servername/keepalived/keepalived.conf.lvs1",
            backup => ".bak",
          
          }

}

        lvs2: {
            file {
            "/etc/keepalived/keepalived.conf":
            mode  => 644,
            owner => root,
            group => root,
            ensure => present,
            source => "puppet://$servername/keepalived/keepalived.conf.lvs2",
            backup => ".bak",

          }

}

        node29: {
            file {
            "/etc/keepalived/keepalived.conf":
            mode  => 644,
            owner => root,
            group => root,
            ensure => present,
            source => "puppet://$servername/keepalived/keepalived.conf.lvs1",
            backup => ".bak",

          }

}

}


      service {
        "keepalived":
        enable    => "true",
        ensure    => "running",
        subscribe => File["/etc/keepalived/keepalived.conf"],
    }
     
}
