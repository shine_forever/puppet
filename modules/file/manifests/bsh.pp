class file::bsh {
                  file { "/usr/local/bsh": 
                         ensure => "directory", 
                         owner => "root",
                         group => "root",
                         mode => 0755,
#recurse => "true"；递归，同步sbin下所有内容；
                         recurse => "true",
#purge => true,参数代表目录中文件完全同步，如果client端相关目录的文件，server没有，puppet会删除该目录下文件；
                         purge => true,
#如果/usr/local/shell/目录下有子目录，以下参数就可以强制删除；
                         force => true,
                         source => "puppet://$servername/file/bsh/",
                         backup => ".bak",
                        }

}

