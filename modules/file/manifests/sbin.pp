class file::sbin {
                  file { "/usr/local/sbin": 
                         ensure => "directory", 
                         owner => "root",
                         group => "root",
                         mode => 0755,
#recurse => "true"；递归，同步sbin下所有内容；
                         recurse => "true",
                         source => "puppet://$servername/file/sbin/",
                         backup => ".bak",
                        }

}

