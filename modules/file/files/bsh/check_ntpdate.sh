#! /bin/bash
#writted by guoeltao;
#脚本说明：同步时间，并且检测命令是否成功；

#设置ntp服务器地址；
NTPSERVER=192.168.1.71
#设置发件邮箱；
Mailaddress=erpadmin@routon.com
Mailserver=mail.routon.com
USERNAME=erpadmin
PASSWORD=erpadmin
#设置收信人；
TOmailer1=guoletao@routon.com
TOmailer2=zhangrunqi@routon.com
#同步时间；
/usr/sbin/ntpdate $NTPSERVER
#判断命令是否允许成功，如果命令执行成功，$?返回结果为0；
if [ "$?" == "0" ];then
    /sbin/hwclock -w
else
    /usr/local/bin/sendEmail -f $Mailaddress -s $Mailserver -xu $USERNAME -xp $PASSWORD -u "Can't ntpdated on host:"$HOSTNAME \
    -m "ntpdate error pls check again!!!" -t $TOmailer1 -cc $TOmailer2
fi
#取消变量；
unset NTPSERVER
unset Mailaddress
unset Mailserver
unset USERNAME
unset PASSWORD
unset TOmailer1
unset TOmailer2
