import "*.pp"

class logclient {
    case $lsbmajdistrelease {
        5: { include syslog }
        6: { include rsyslog }
    }
}
