class logclient::rsyslog {
    package { "rsyslog":
               ensure => present,
    }

    file { "/etc/rsyslog.conf":
            owner   => "root",
            group   => "root",
            mode    => "644",
            require   => Package["rsyslog"],
#指定syslog.conf文件同步的具体路径；
            source => "puppet://$servername/logclient/rsyslog.conf",
            backup => ".bak",
    }


    service {
        "rsyslog":
#syslog服务器开机自启动
        enable    => "true",
#保证syslog处于运行状态
        ensure    => "running",
#puppet知道程序的状态，运行状态还是停止；
        hasstatus => "true",
        pattern   => "rsyslog",
        require   => File["/etc/rsyslog.conf"],
        subscribe => File["/etc/rsyslog.conf"],
    }

}
