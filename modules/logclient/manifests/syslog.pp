class logclient::syslog {
    package { "sysklogd":
               ensure => present,
    }

    file { "/etc/syslog.conf":
            owner   => "root",
            group   => "root",
            mode    => "644",
            require   => Package["sysklogd"],
#指定syslog.conf文件同步的具体路径；
            source => "puppet://$servername/logclient/syslog.conf",
            backup => ".bak",
    }


    service {
        "syslog":
#syslog服务器开机自启动
        enable    => "true",
#保证syslog处于运行状态
        ensure    => "running",
#puppet知道程序的状态，运行状态还是停止；
        hasstatus => "true",
        pattern   => "syslog",
        require   => File["/etc/syslog.conf"],
        subscribe => File["/etc/syslog.conf"],
    }

}
