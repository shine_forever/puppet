import "*.pp"

class yum {


    if $operatingsystem == "Redhat" {

            package { ["yum"]:ensure => present; }

                  if $lsbdistrelease == "5.4" {
                       include yum::rhel54 
                  }

                  if $lsbdistrelease == "5.7" {      
                  
                      include yum::rhel57

                               }

                  if $lsbdistrelease == "6.0" {

                      include yum::rhel60

                               }
                  }
                  
}            


