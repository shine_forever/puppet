class yum::rhel57 {
             case $architecture {
             i386: {
                 file {
                   "/etc/yum.repos.d/rhel5.7_i386.repo":
                    mode => 644, owner => root, group => root,
                    ensure => present,
                    source => "puppet://$servername/yum/rhel5.7_i386.repo",
                   } 
             }


         


             x86_64: {
                 file {
                   "/etc/yum.repos.d/rhel5.7_x86_64.repo":
                    mode => 644, owner => root, group => root,
                    ensure => present,
                    source => "puppet://$servername/yum/rhel5.7_x86_64.repo",
                   }
             }



    }
  }
