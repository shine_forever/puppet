class yum::rhel60 {
             case $architecture {
             i386: {
                 file {
                   "/etc/yum.repos.d/rhel6.0_i386.repo":
                    mode => 644, owner => root, group => root,
                    ensure => present,
                    source => "puppet://$servername/yum/rhel6.0_i386.repo",
                   }
             }





             x86_64: {
                 file {
                   "/etc/yum.repos.d/rhel6.0_x86_64.repo":
                    mode => 644, owner => root, group => root,
                    ensure => present,
                    source => "puppet://$servername/yum/rhel6.0_x86_64.repo",
                   }
             }



    }
  }
