class snmpd {
    service {
        "snmpd":
        enable    => "true",
        ensure    => "running",
        subscribe => File["/etc/snmp/snmpd.conf","/etc/sysconfig/snmpd.options"],
        require   =>  Package["net-snmp"],
    }

    package {
        "net-snmp":
         ensure => present,
    }
    
    file { 
            "/etc/snmp/snmpd.conf":
             mode   => 644, 
             owner  => root, 
             group  => root,
             ensure => present,
             source => "puppet://$servername/snmpd/snmpd.conf",
             backup => ".bak",
         }

    file {
            "/etc/sysconfig/snmpd.options":
             mode   => 755,
             owner  => root,
             group  => root,
             ensure => present,
             source => "puppet://$servername/snmpd/snmpd.options",
             backup => ".bak",
    }
}
