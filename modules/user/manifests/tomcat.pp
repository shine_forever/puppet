class user::tomcat {
 user { "tomcat":
     uid => 506,
     gid => tomcat,
     ensure => present,
     shell =>"/bin/bash",
     home  => "/home/tomcat",
#     managehome => true 保证新建用户的时候会创建该用户的home目录；
     managehome => true,
     comment =>"tomcat services",
#     password => '$1$Nnh0M0$t9s7Bbwx2fFer6IP/QGdA0',
     require => group["tomcat"],

       }



 group {
      "tomcat":
       ensure => present,
       gid => 507,
       }

}

