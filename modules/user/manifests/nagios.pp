class user::nagios {
 user {
     "nagios":
     gid => nagios,
     uid => 510,
     ensure   => present,
     shell =>"/bin/bash",
     comment =>"nagios services",
     require => group["nagios"],

       }


 group {
      "nagios":
      gid => 511,
      ensure   => present,
      }
}
