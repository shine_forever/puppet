class user::nginx {
 user {
     "nginx":
     gid => nginx,
     uid => 509,
     ensure   => present,
     shell =>"/bin/bash",
     home  => "/home/nginx",
#managehome => true 保证新建用户的时候会创建该用户的home目录；
     managehome => true,
     comment =>"nginx services",
#设置password，需要系统安装ruby-shadow包；
#     password => '$1$Nnh0M0$t9s7Bbwx2fFer6IP/QGdA0',
     require => group["nginx"],
       }

 group {"nginx":
       gid => 510,
       ensure => present,
     
      }
}
