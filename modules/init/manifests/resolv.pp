class init::resolv {

    file { "/etc/resolv.conf":
            mode   => 644, 
            owner  => root, 
            group  => root,
            ensure => present,
            source => "puppet://$servername/init/resolv.conf",
            backup => ".bak",
    }
}
