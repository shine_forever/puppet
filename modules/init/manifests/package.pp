class init::package {
#定义packages数组
$packages=["xinetd",
           "readline-devel",
           "readline",
           "gcc",
           "gcc-c++",
           "binutils",
           "zlib-devel",
           "openssl",
           "openssl-devel",
           "screen",
           "autoconf",
           "automake",
           "lrzsz",
           "sysstat"]

package { $packages:
           ensure => present,
                }

}
