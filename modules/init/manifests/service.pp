class init::service {
        $serlist = [
		"yum-updatesd",
                "rpcgssd",
                "cups",
                "rhnsd",
                "sendmail",
                "haldaemon",
                "bluetooth",
                "iptables",]


        service
        { $serlist:
          ensure => stopped,
          enable => false,
        }
}
