class hosts::h3test {

host { 'node25.puppet.com': ip => '172.16.41.25', target => "/etc/hosts", ensure => 'present', }
host { 'node26.puppet.com': ip => '172.16.41.26', target => "/etc/hosts", ensure => 'present', }
host { 'node27.puppet.com': ip => '172.16.41.27', target => "/etc/hosts", ensure => 'present', }
host { 'node28.puppet.com': ip => '172.16.41.28', target => "/etc/hosts", ensure => 'present', }
host { 'node29.puppet.com': ip => '172.16.41.29', target => "/etc/hosts", ensure => 'present', }
host { 'node222.puppet.com': ip => '172.16.41.222', target => "/etc/hosts", ensure => 'present', }

}
