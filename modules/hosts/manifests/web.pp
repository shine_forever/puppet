class hosts::web {

host { 'test.routon.com': ip => '192.168.0.11', host_aliases => [ "pay1", "payment1" ], target => "/etc/hosts", ensure => 'absent', }
host { 'test2.routon.com': ip => '192.168.0.12', host_aliases => [ "pay2", "payment2" ], target => "/etc/hosts", ensure => 'absent', }
host { 'test3.routon.com': ip => '192.168.0.13', host_aliases => [ "pay3", "payment3" ], target => "/etc/hosts", ensure => 'absent', }
host { 'test4.routon.com': ip => '192.168.0.14', host_aliases => [ "pay4", "payment4" ], target => "/etc/hosts", ensure => 'absent', }
host { 'guo.routon.com': ip => '192.168.0.14', host_aliases => [ "pay4", "payment4" ], target => "/etc/hosts", ensure => 'present', }


}
